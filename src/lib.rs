#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod tests {
    use libc::c_void;
    use std::ptr;

    use super::*;

    #[test]
    fn sanity() {
        extern "C" fn eh_main(_rt: *mut EhRt, _data: *mut c_void) {
            println!("hello from the other side");
        }

        unsafe {
            let info = EhInfo {
                thread_id: 0,
                stack: 0x10000,
                heap: 0x10000,
                argc: 0,
                argv: ptr::null_mut(),
                data: ptr::null_mut(),
            };
            let rt = new_rt(ptr::null_mut(), &info);
            del_rt(rt);
            ehrt_start(Some(eh_main), &info);
        }
    }
}
