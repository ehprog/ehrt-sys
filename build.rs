use std::env;
use std::path::PathBuf;

const INCLUDE: &str = concat!(env!("HOME"), "/.local/include");
#[cfg(target_pointer_width = "64")]
const LIB: &str = "lib64";
#[cfg(target_pointer_width = "32")]
const LIB: &str = "lib";

fn main() {
    println!("cargo:rustc-link-lib=ehrt");
    println!("cargo:rustc-link-search=all={}/.local/{}", env!("HOME"), LIB);

    let bindings = bindgen::Builder::default()
        .clang_arg(format!("-I{}", INCLUDE))
        .header("ehrt.h")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
